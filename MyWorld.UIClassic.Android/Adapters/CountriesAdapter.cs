﻿using Android.App;
using Android.Views;
using Android.Widget;
using MyWorld.Common.Models;
using MyWorld.UIClassic.Android.GoogleMap;
using System.Collections.Generic;

namespace MyWorld.UIClassic.Android.Adapters
{
    public class CountriesAdapter : BaseAdapter<Country>
    {
        private readonly List<Country> items;
        private readonly Activity context;
        private GoogleMapRepository googleMapRepository;
        public CountriesAdapter(Activity context, List<Country> items) : base()
        {
            this.context = context;
            this.items = items;
            googleMapRepository = new GoogleMapRepository();
        }

        public override Country this[int position] => items[position];

        public override int Count => items.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            if (convertView == null)
                convertView = context.LayoutInflater.Inflate(Resource.Layout.CountryRow, null);

            View view = convertView;
            convertView.FindViewById<TextView>(Resource.Id.nameTextView).Text = item.Name;
            convertView.FindViewById<TextView>(Resource.Id.capitalTextView).Text = item.Capital;
            convertView.FindViewById<TextView>(Resource.Id.regionTextView).Text = item.Region;
            ImageButton btnSelect = view.FindViewById<ImageButton>(Resource.Id.goToMapButton);

            btnSelect.Click += delegate
            {
                googleMapRepository.GoGoogleMap(context, items[position].Latlng);
            };

            return convertView;
        }
    }
}