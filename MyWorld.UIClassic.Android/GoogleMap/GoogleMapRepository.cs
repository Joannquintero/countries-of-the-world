﻿using Android.Content;
using Android.Widget;
using System.Collections.Generic;
using Uri = Android.Net.Uri;

namespace MyWorld.UIClassic.Android.GoogleMap
{
    using AndroidUri = Uri;
    public class GoogleMapRepository
    {
        public void GoGoogleMap(Context context, List<double> latlng)
        {
            if (latlng.Count > 0)
            {
                double[] coordinates = latlng.ToArray();
                var geoUri = AndroidUri.Parse($"geo:{coordinates[0]},{coordinates[1]}");
                var mapIntent = new Intent(Intent.ActionView, geoUri);
                context.StartActivity(mapIntent);
            }
            else
            {
                Toast.MakeText(context, context.GetString(Resource.String.no_coordinates), ToastLength.Long).Show();
            }     
        }
    }
}