﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MyWorld.UIClassic.Android.Data
{
    public class UserDatabase : SQLiteConnection
    {
		static object locker = new object();

		public static string DatabaseFilePath
		{
			get
			{
				var sqliteFilename = "UsersDB.db3";
#if __ANDROID__
				string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); ;
#else
				// we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
				// (they don't want non-user-generated data in Documents)
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				string libraryPath = Path.Combine (documentsPath, "../Library/"); // Library folder
#endif
				var path = Path.Combine(libraryPath, sqliteFilename);

				return path;
			}
		}

		public UserDatabase(string path) : base(path)
		{
			CreateTable<User>();
		}

		public IEnumerable<User> GetUsers()
		{
			lock (locker)
				return (from i in Table<User>() select i).ToList();
		}

		public User GetUser(int id)
		{
			lock (locker)
				return Table<User>().FirstOrDefault(x => x.Id == id);
		}

		public int SaveUser(User item)
		{
			lock (locker)
			{
				if (item.Id != 0)
				{
					Update(item);
					return item.Id;
				}
				else
				{
					return Insert(item);
				}
			}
		}

		public int DeleteUser(User stock)
		{
			lock (locker)
				return Delete<User>(stock.Id);
		}
	}
}