﻿using SQLite;

namespace MyWorld.UIClassic.Android.Data
{
    public class User
    {
		[PrimaryKey, AutoIncrement, Column("_id")]
		public int Id { get; set; }

		[MaxLength(64)]
		public string UserName { get; set; }

		[MaxLength(32)]
		public string Password { get; set; }
	}
}