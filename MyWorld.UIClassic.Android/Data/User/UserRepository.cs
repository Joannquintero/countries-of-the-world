﻿using System.Collections.Generic;

namespace MyWorld.UIClassic.Android.Data
{
    public class UserRepository
    {
		private UserDatabase db = null;
		protected static UserRepository me;
		static UserRepository()
		{
			me = new UserRepository();
		}
		protected UserRepository()
		{
			db = new UserDatabase(UserDatabase.DatabaseFilePath);
		}

		public static User GetUser(int id)
		{
			return me.db.GetUser(id);
		}

		public static IEnumerable<User> GetUsers()
		{
			return me.db.GetUsers();
		}

		public static int SaveUser(User item)
		{
			return me.db.SaveUser(item);
		}

		public static int DeleteUser(User item)
		{
			return me.db.DeleteUser(item);
		}
	}
}