﻿using MyWorld.UIClassic.Android.Helpers;
using System.Linq;

namespace MyWorld.UIClassic.Android.Data
{
    public class SeedDb
    {
        private readonly string userName;
        private readonly string password;
        public SeedDb()
        {
            userName = "user@intergrupo.com";
            password = "123456";
        }
        public int CheckUser()
        {
            var user = UserRepository.GetUsers().Where(u => u.UserName == userName).FirstOrDefault();
            if (user == null)
            {
                return UserRepository.SaveUser(new User
                {
                    UserName = userName,
                    Password = ConvertBase64Encode.Base64Encode(password)
                });
            }

            return user.Id;
        }
    }
}