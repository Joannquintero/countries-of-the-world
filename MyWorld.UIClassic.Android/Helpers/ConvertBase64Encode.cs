﻿using System;

namespace MyWorld.UIClassic.Android.Helpers
{
    public static class ConvertBase64Encode
    {
        public static string Base64Encode(string text)
        {
            var TextBytes = System.Text.Encoding.UTF8.GetBytes(text);
            return Convert.ToBase64String(TextBytes);
        }
    }
}