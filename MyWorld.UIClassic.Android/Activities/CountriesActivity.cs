﻿
using Android.App;
using Android.OS;
using Android.Widget;
using MyWorld.Common.Models;
using MyWorld.Common.Services;
using MyWorld.UIClassic.Android.Adapters;
using MyWorld.UIClassic.Android.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyWorld.UIClassic.Android.Activities
{
    [Activity(Label = "@string/countries", Theme = "@style/AppTheme")]
    public class CountriesActivity : Activity
    {
        #region attributes
        private ApiService apiService;
        private ListView countriesListView;
        private EditText SearchCountries;
        private ImageButton searchButton;
        private string auth;
        #endregion end attributes

        public List<Country> Countries { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.CountriesPage);
            countriesListView = FindViewById<ListView>(Resource.Id.countriesListView);
            SearchCountries = FindViewById<EditText>(Resource.Id.inputSearch);
            searchButton = FindViewById<ImageButton>(Resource.Id.searchButton);
            auth = Intent.Extras.GetString("auth");
            apiService = new ApiService();
            CountriesHandle();
            searchButton.Click += (sender, e) => { Filter(); };
            SearchCountries.TextChanged += (sender, e) => { Filter(); };
        }

        private void Filter()
        {
            string searchText = SearchCountries.Text.ToLower();
            List<Country> list = Countries.Where(c => c.Name.ToLower().Contains(searchText) ||
            c.Capital.ToLower().Contains(searchText) ||
            c.Region.ToLower().Contains(searchText)).ToList();
            countriesListView.Adapter = new CountriesAdapter(this, list);
            countriesListView.FastScrollEnabled = true;
        }

        private async void CountriesHandle()
        {
            var countries = await GetCountries();
            countriesListView.Adapter = new CountriesAdapter(
                this, 
                countries.OrderBy(c => c.Name).ToList());
            countriesListView.FastScrollEnabled = true;
        }

        public async Task<List<Country>> GetCountries()
        {
            string key = Resources.GetString(Resource.String.apiKey);
            var response = await apiService.GetCountriesAsync<Country>(
                GetString(Resource.String.urlBase),
                "/all",
                key);

            if (!response.IsSuccess)
            {
                DialogService.ShowMessage(
                    this, 
                    GetString
                    (Resource.String.app_name), 
                    response.Message, 
                    GetString(Resource.String.accept));
                return new List<Country>();
            }

            Countries = (List<Country>)response.Result;
            return Countries;
        }
    }
}