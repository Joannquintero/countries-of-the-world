﻿
using Android.App;
using Android.Content;
using Android.OS;
using System.Threading.Tasks;

namespace MyWorld.UIClassic.Android.Activities
{
    [Activity(
        Theme = "@style/MyTheme.Splash",
        MainLauncher = true,
        NoHistory = true)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        protected override void OnResume()
        {
            base.OnResume();
            Task startupWork = new Task(() => { InitialAction(); });
            startupWork.Start();
        }

        private async void InitialAction()
        {
            await Task.Delay(5000);
            StartActivity(new Intent(
                Application.Context, 
                typeof(LoginActivity)));
        }
    }
}