﻿
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using MyWorld.Common.Models;
using MyWorld.UIClassic.Android.Data;
using MyWorld.UIClassic.Android.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyWorld.UIClassic.Android.Activities
{

    [Activity(
        Label = "@string/app_name", 
        Theme = "@style/AppTheme")]
    public class LoginActivity : AppCompatActivity
    {
        #region attributes
        private SeedDb seedDb;
        private EditText emailText;
        private EditText passwordText;
        private ProgressBar activityIndicator;
        private Button loginButton;
        protected IList<User> users;
        protected ListView taskListView = null;
        #endregion end attributes

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.LoginPage);
            ControlAssignment();
            HandleEvents();
            SetInitialData();
        }

        protected override void OnResume()
        {
            base.OnResume();
            users = UserRepository.GetUsers().ToList();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private void SetInitialData()
        {
            seedDb = new SeedDb();
            seedDb.CheckUser();
            activityIndicator.Visibility = ViewStates.Invisible;
        }

        private void HandleEvents()
        {
            loginButton.Click += (sender, e) => { Authenticate(); }; ;
        }

        protected void Authenticate()
        {
            if (string.IsNullOrEmpty(emailText.Text))
            {
                DialogService.ShowMessage(
                    this, 
                    GetString(Resource.String.app_name), 
                    $"{GetString(Resource.String.request_user)}.", 
                    GetString(Resource.String.accept));
                return;
            }

            if (string.IsNullOrEmpty(passwordText.Text))
            {
                DialogService.ShowMessage(
                    this, 
                    GetString(Resource.String.app_name), 
                    $"{GetString(Resource.String.request_password)}.", 
                    GetString(Resource.String.accept));
                return;
            }

            activityIndicator.Visibility = ViewStates.Visible;
            
            var user = UserRepository.GetUsers()
                .Where(u => u.UserName == emailText.Text.ToLower() 
                && u.Password == ConvertBase64Encode.Base64Encode(passwordText.Text))
                .FirstOrDefault();
            if (user == null)
            {
                activityIndicator.Visibility = ViewStates.Invisible;
                DialogService.ShowMessage(
                    this, 
                    GetString(Resource.String.app_name), 
                    $"{GetString(Resource.String.invalid_authentication)}.", 
                    GetString(Resource.String.accept));
                return;
            }

            loginButton.Enabled = false;
            var intent = new Intent(this, typeof(CountriesActivity));
            intent.PutExtra("auth", JsonConvert.SerializeObject(new AuthResponse
            {
                Id = Guid.NewGuid(),
                UsarName = user.UserName,
                Date = DateTime.UtcNow,
                Expires = DateTime.UtcNow.AddDays(30)
            }));
            StartActivity(intent);
            Finish();
        }


        private void ControlAssignment()
        {
            emailText = FindViewById<EditText>(Resource.Id.emailText);
            passwordText = FindViewById<EditText>(Resource.Id.passwordText);
            activityIndicator = FindViewById<ProgressBar>(Resource.Id.activityIndicatorProgressBar);
            loginButton = FindViewById<Button>(Resource.Id.loginButton);
        }
    }
}