﻿using MyWorld.Common.Models;
using System.Threading.Tasks;

namespace MyWorld.Common.Services
{
    public interface IApiService
    {
        Task<Response> GetCountriesAsync<T>(
            string urlBase,
            string action,
            string apiKey);
    }
}
