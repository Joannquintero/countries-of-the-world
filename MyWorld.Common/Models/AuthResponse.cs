﻿using System;

namespace MyWorld.Common.Models
{
    public class AuthResponse
    {
        public Guid Id { get; set; }
        public string UsarName { get; set; }
        public DateTime Date { get; set; }
        public DateTime Expires { get; set; }
    }
}
